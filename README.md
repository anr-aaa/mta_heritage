# Artwork recommendations guided by foundation models: survey and novel approach

## Description
This repository contains the materials presented in the journal paper 'Artwork recommendations guided by foundation models: survey and novel approach'. 

<a href="https://liris.cnrs.fr/page-membre/tetiana-yemelianenko">Tetiana Yemelianenko</a>,
<a href="https://liris.cnrs.fr/page-membre/iuliia-tkachenko">Iuliia Tkachenko</a>,
<a href="https://liris.cnrs.fr/page-membre/tess-masclef">Tess Masclef</a>,
<a href="https://liris.cnrs.fr/page-membre/mihaela-scuturici">Mihaela Scuturici</a>,
<a href="https://liris.cnrs.fr/page-membre/serge-miguet">Serge Miguet</a>

<div style="text-align:center"><img style="margin-right: 20px" src="assets/fig8.png" alt="Recommendations" height="325" width="660"/>

The code of notebook 'Retrieve_combined_features.ipynb' illustrates the combination of recommendations with the same weights of genre, style and artist criteria, it can be easily modified for using with different weights.
The code for fine-tuning a classification model is based on the <a href="https://github.com/huggingface/peft/tree/main/examples/image_classification">example</a>. 

# Table of content
- [Overview](#description)
- [Dataset](#dataset)
- [Steps for reproducing the process of dataset creation](#steps)
- [Citation](#citation)
- [Acknowledgements](#acknowledgments)

## Dataset

The WikiArt dataset used for fine-tuning could be found <a href="https://huggingface.co/datasets/huggan/wikiart">here</a>. Before fine-tunibg, dataset should be downloaded and filtered for excluding 'Unknown genre' class for genre classification fine-tuning and for selecting artists from the list of most popular artists for artist classification fine-tuning. For the style classification fine-tuning original dataset could be used.

## Steps
To reproduce the steps first you need to finetune models for genre, style and artist classification on downloaded WikiArt dataset or use fine-tuned adapters (folder LoRA adapters). Then, using fine-tuned models you need to calculate embeddings for the images from WikiArt dataset and create ANNOY indexes for each fine-tuned model. Due to the relatively big size, these files (embeddings and ANNOY indexes) are available upon a request.

## Citation
```
@article{Yemelianenko_2024_MTA,
    author    = {Yemelianenko, Tetiana and Tkachenko, Iuliia and Masclef, Tess and Scuturici, Mihaela and Miguet, Serge},
    title     = {Artwork recommendations guided by foundation models: survey and novel approach},
    journal = {},
    month     = {},
    year      = {},
    pages     = {}
    issue_date = {},
    publisher = {},
    address = {}
}
```

## Acknowledgments		
This work was funded by french national research agency with grant ANR-20-CE38-0017. We would like to thank the PAUSE ANR-Program: Ukrainian scientists support to support the scientific stay of T. Yemelianenko in LIRIS laboratory.
<div style="text-align:center"><img style="margin-right: 20px" src="assets/logo_liris.png" alt="LIRIS logo" height="75" width="160"/>
<div style="text-align:center"><img style="margin-right: 20px" src="assets/anr-logo.png" alt="ANR logo" height="75" width="160"/>



